package com.example.demo.services;

import com.example.demo.web.model.Person;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface PersonServices {

    public Optional<Person> getPersonById(int personId);

    public List<Person> getAllPersons();

    public boolean removePerson(int personId);

    public Optional<Person> saveUpdatePerson(Person person);

}
