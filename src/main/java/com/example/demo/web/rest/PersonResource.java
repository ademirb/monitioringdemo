package com.example.demo.web.rest;

import com.example.demo.services.PersonServices;
import com.example.demo.web.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonResource {

    private final PersonServices personService;

    @Autowired
    PersonResource(PersonServices personServices){
        this.personService = personServices;
    }


    @GetMapping
    public List<Person> getAllPerson() {
        return personService.getAllPersons();
    }

    @GetMapping(value = "/{personId}")
    public ResponseEntity<Person> getPerson(@PathVariable("personId") int personId) {
        return personService.getPersonById(personId).map(person -> {
            return ResponseEntity.ok(person);
        }).orElseGet(() -> {
            return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
        });
    }


    @PostMapping
    public ResponseEntity<Person> addNewPerson(@RequestBody Person person) {
        return personService.saveUpdatePerson(person).map(p -> {
            return ResponseEntity.ok(p);
        }).orElseGet(() -> {
            return new ResponseEntity<Person>(HttpStatus.EXPECTATION_FAILED);
        });
    }

    @PutMapping
    public ResponseEntity<Person> updatePerson(@RequestBody Person person) {
        return personService.saveUpdatePerson(person).map(p -> {
            return ResponseEntity.ok(p);
        }).orElseGet(() -> {
            return new ResponseEntity<Person>(HttpStatus.EXPECTATION_FAILED);
        });
    }


    @DeleteMapping(value = "/{personId}")
    public ResponseEntity<String> deletePerson(@PathVariable("personId") int personId) {
        if (personService.removePerson(personId)) {
            return ResponseEntity.ok("Person with id : " + personId + " removed");
        } else {
            return new ResponseEntity<String>("Error deleting enitty ", HttpStatus.EXPECTATION_FAILED);
        }
    }

}
